#ifndef _INQUEST_TESTRESULT_H
#define _INQUEST_TESTRESULT_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _INQUEST_TESTCASE_H
#include <inquest/TestCase.h>
#endif

#ifndef _INQUEST_TESTSTATE_H
#include <inquest/TestState.h>
#endif

#include <iosfwd>
#include <string>

namespace inquest {
  /**
   * A test case is used to test a function or class method with 
   * a specific set of inputs and a defined set of outputs.
   */
  class TestResult : public ::timber::Counted {
    /** No copying allowed */
    TestResult(const TestResult&);
    TestResult&operator=(const TestResult&);
    
    /** 
     * constructor 
     * @param tc the testcase 
     * @param out the outcome of the testcase
     * @param msg a message
     */
  private:
    TestResult(::timber::Reference<TestCase> tc, TestState out, const ::std::string& msg);
      
    /** The destructor */
  public:
    ~TestResult() throws ();

    /** 
     * constructor 
     * @param tc the testcase 
     * @param out the outcome of the testcase
     * @param msg a message
     * @param e an exception that occurred
     */
  public:
    static ::timber::Reference<TestResult> create (::timber::Reference<TestCase> tc, TestState out, const ::std::string& msg, const ::std::exception& e) ;
      
    /** 
     * constructor 
     * @param tc the testcase 
     * @param out the outcome of the testcase
     * @param msg a message
     * @throws IllegalArgumentException if tc==0
     */
  public:
    static ::timber::Reference<TestResult> create  (::timber::Reference<TestCase> tc, TestState out, const ::std::string& msg) ;
      
    /** 
     * constructor 
     * @param tc the testcase 
     * @param out the outcome of the testcase
     * @throws IllegalArgumentException if tc==0
     */
  public:
    static ::timber::Reference<TestResult> create  (::timber::Reference<TestCase> tc, TestState out) ;
      
    /**
     * Serialize this testcase into an output stream.
     * @param stream an output stream
     * @throws ::runtime_error if the serialization failed
     */
  public:
    virtual void serialize (::std::ostream& stream) const throws (::std::runtime_error);
    
    /**
     * Deserialize a test result from an input stream
     * @param stream an input stream
     * @return a reference to a TestResult or 0 if nothing was read from the stream
     * @throws ::runtime_error if a test result could not be read
     */
  public:
    static ::timber::Reference<TestResult> read (::std::istream& stream) throws (::std::runtime_error);
			     
    /**
     * Get the testcase
     * @return the testcase 
     */
  public:
    ::timber::Reference<TestCase> testCase() const throws ();
      
    /**
     * Get the outcome of the test
     * @return the test outcome
     */
  public:
    TestState outcome() const throws ();

    /**
     * Get the message.
     * @return the message
     */
  public:
    ::std::string message () const throws ();

    /** The testcase */
  private:
    ::timber::Reference<TestCase> _testcase;

    /** The outcome */
  private:
    const TestState _outcome;

    /** A simple message */
  private:
    const ::std::string _message;
  };
    
}


#endif

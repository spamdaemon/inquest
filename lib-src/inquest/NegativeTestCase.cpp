#include <inquest/NegativeTestCase.h>

namespace inquest {
  namespace {
    TestState negateTestState (TestState c)
    {
      switch (c.value()) {
      case TestState::PASSED:
	return TestState::FAILED;
      case TestState::FAILED:
	return TestState::PASSED;
      default:
	return TestState::UNKNOWN;
      };
    }
  }

  NegativeTestCase::NegativeTestCase (::timber::Reference<TestCase> tc, const ::std::string& tid) throws(::std::invalid_argument)
    : _testcase(tc),
      _outcome(negateTestState(tc->expectedOutcome())),
      _testID(tid)
  {
    if (tid.empty()) {
      throw ::std::invalid_argument("No test case id");
    }
  }
    
  NegativeTestCase::~NegativeTestCase() throws()
  {}

  ::timber::Reference<TestCase> NegativeTestCase::create (::timber::Reference<TestCase> ref, const ::std::string& tid) throws(::std::invalid_argument)
  { return new NegativeTestCase(ref,tid); }
    
  void NegativeTestCase::doTest()
  { _testcase->doTest(); }


  TestState NegativeTestCase::expectedOutcome() const throws()
  { return _outcome; }
    
  ::std::string NegativeTestCase::id() const throws()
  { return _testID; }
    
}

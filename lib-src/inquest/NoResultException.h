#ifndef _INQUEST_NORESULTEXCEPTION_H
#define _INQUEST_NORESULTEXCEPTION_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <stdexcept>

namespace inquest {
  /**
   * This exception is thrown to signal that a testcase has not
   * produced a meaningful outcome.
   */
  class NoResultException : public ::std::runtime_error {
    
    /** 
     * Create a new exception.
     * @param msg a message
     */
  public:
    NoResultException (const ::std::string& msg) throws ();

    /** Destructor */
  public:
    ~NoResultException() throw() {}


  };
}

#endif

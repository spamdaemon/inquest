#include <inquest/TestCase.h>

namespace inquest {
  TestCase::TestCase () throws()
  {}
  
  TestCase::~TestCase() throws()
  {}

  TestState TestCase::expectedOutcome() const throws()
  { return TestState::PASSED; }
  
}

#ifndef _INQUEST_SIMPLETESTRESULTS_H
#define _INQUEST_SIMPLETESTRESULTS_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _INQUEST_TESTRESULTS_H
#include <inquest/TestResults.h>
#endif

#include <vector>

namespace inquest {
  /**
   * TestResults holds the results from running multiple tests.
   */
  class SimpleTestResults : public TestResults {
    /** No copying allowed */
    SimpleTestResults(const SimpleTestResults&);
    SimpleTestResults&operator=(const SimpleTestResults&);

    /** 
     * Default constructor 
     */
  public:
    SimpleTestResults() throws ();
      
    /** The destructor */
  public:
    ~SimpleTestResults() throws ();

    /**
     * Add a test result
     * @param result a test result
     */
  public:
    void addResult (::timber::Reference<TestResult> result);
    
  public:
    ::std::vector< ::timber::Reference<TestResult> > exportResults() const throws ();
    
    /** The vector of test resutls */
  private:
    ::std::vector< ::timber::Reference<TestResult> > _results;
  };
}


#endif

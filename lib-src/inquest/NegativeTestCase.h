#ifndef _INQUEST_NEGATIVETESTCASE_H
#define _INQUEST_NEGATIVETESTCASE_H

#ifndef _INQUEST_TESTCASE_H
#include <inquest/TestCase.h>
#endif

#ifndef _INQUEST_TESTSTATE_H
#include <inquest/TestState.h>
#endif

#include <string>

namespace inquest {

    /**
     * This negative test case takes a test case and negates its result. The result of 
     * original test case will be mapped into an new expected result as follows:
     * <pre>
     *   PASSED  -> FAILED
     *   FAILED  -> PASSED
     *   UNKNOWN -> UNKNOWN   
     * </pre>
     */
    class NegativeTestCase : public TestCase {
      /** No copying allowed */
    NegativeTestCase(const NegativeTestCase&);
    NegativeTestCase&operator=(const NegativeTestCase&);

      /**
       * Constructor.
       * @param tc a testcase
       * @param tid a test id
       * @throws ::std::invalid_argument if tid.empty()
       */
    private:
      NegativeTestCase (::timber::Reference<TestCase> tc, const ::std::string& tid) throws (::std::invalid_argument);
      
      /** The destructor */
    public:
      ~NegativeTestCase() throws ();

      /**
       * Create a negative test case
       * @param tc a test case
       * @param tid a test id
       * @pre REQUIRE_NON_ZERO(ref)
       * @pre REQUIRE_NON_ZERO(tid)
       * @return a test case
       */
    public:
      static ::timber::Reference<TestCase> create (::timber::Reference<TestCase> tc, const ::std::string& tid) throws (::std::invalid_argument);

      /**
       * Create a negative test case with the same id as the specified test case.
       * @param tc a test case
       * @return a test case
       * @throws ::std::invalid_argument if tid.empty()
       */
    public:
      static ::timber::Reference<TestCase> create (::timber::Reference<TestCase> tc) throws (::std::invalid_argument)
	{ return create(tc,tc->id()); }

      /**
       * Execute this test case. Calls
       * @throws any exception if no result could be obtained
       */
    public:
      void doTest();

      /**
       * Get the expected outcome. Resu
       * @return Outcome::PASSED
       */
    public:
      TestState expectedOutcome() const throws ();

      /**
       * Get the id.
       * @return the id
       */
    public:
      ::std::string id() const throws ();

      /** The testcase to be negated */
    private:
      ::timber::Reference<TestCase> _testcase;

      /** The expected outcome */
    private:
      const TestState _outcome;
      
      /** The test id */
    private:
      const ::std::string _testID;

    };
    
  }


#endif

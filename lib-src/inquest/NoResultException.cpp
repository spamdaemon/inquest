#include <inquest/NoResultException.h>

namespace inquest {
  NoResultException::NoResultException (const ::std::string& s) throws()
    : ::std::runtime_error(s) 
  {}

}

#include <inquest/TestResult.h>
#include <iostream>

namespace inquest {
  TestResult::TestResult (::timber::Reference<TestCase> tc, TestState out, const ::std::string& msg)
    : _testcase(tc),_outcome(out),_message(msg)
  {}

  ::timber::Reference<TestResult> TestResult::create (::timber::Reference<TestCase> tc, TestState out, const ::std::string& msg, const std::exception& e)
  { return new TestResult(tc,out,msg + e.what()); }
    
  ::timber::Reference<TestResult> TestResult::create (::timber::Reference<TestCase> tc, TestState out, const ::std::string& msg)
  { return new TestResult(tc,out,msg); }

  ::timber::Reference<TestResult> TestResult::create (::timber::Reference<TestCase> tc, TestState out)
  { return new TestResult(tc,out,""); }

#if 0
  ::timber::Reference<TestResult> TestResult::create (const ::inquest::xml::dom::Element& e) throws(::std::runtime_error)
  {
    if (e==0) {
      throw ::std::runtime_error("Could not read a test result from the input stream");
    }
      
    if (!e.qname().localName().equals("TestResult")) {
      throw ::std::runtime_error("Invalid root node");
    }
    ::inquest::xml::dom::Attribute result = e.getAttribute(::inquest::xml::QName("outcome"));
    ::inquest::xml::dom::Attribute tid = e.getAttribute(::inquest::xml::QName("testcase"));
    if (result==0) {
      throw ::std::runtime_error("TestResult has no outcome");
    }
    if (tid==0) {
      throw ::std::runtime_error("TestResult has no associated test-case");
    }
      
    // create a testcase
    struct LTestCase : public TestCase {
      LTestCase (::std::string xid)
	: _id(xid) {}
      ~LTestCase() throws() {}
      ::std::string id() const throws() { return _id; }
      void doTest() {}
    private:
      ::std::string _id;
    };
      
    ::timber::Reference<TestCase> tc = static_cast<TestCase*>(new LTestCase(tid.value().string()));
    TestState out(TestState::PASSED);
    if (result.value().equals("passed")) {
      out = TestState::PASSED;
    }
    else if(result.value().equals("failed")) {
      out = TestState::FAILED;
    }
    else if (result.value().equals("unknown")) {
      out = TestState::UNKNOWN;
    }
    else {
      throw ::std::runtime_error("Invalid outcome");
    }
    ::std::string msg;
    msg = "";
    ::timber::Reference<TestResult> tr = new TestResult(tc,out,msg);
    return tr;
  }
#endif


  TestResult::~TestResult() throws()
  {}

  ::timber::Reference<TestCase> TestResult::testCase() const throws()
  { return _testcase; }
      
  TestState TestResult::outcome() const throws()
  { return _outcome; }

  ::std::string TestResult::message() const throws()
  { return _message; }


  void TestResult::serialize (::std::ostream& stream) const throws (::std::runtime_error)
  {
    try {
      stream << "<TestResult testcase=\"" 
	     << testCase()->id() 
	     << "\" outcome=\"";
      switch (outcome().value()) {
      case TestState::PASSED:  stream << "passed"; break;
      case TestState::FAILED:  stream << "passed"; break;
      case TestState::UNKNOWN: 
      default:
	stream << "unknown"; break;
      };
      stream << "\">\n";
      stream << "<Message>";
      stream << message();
      stream << "</Message>";
      stream << "</TestResult>\n";
      stream << ::std::flush;
      if (!stream.good()) {
	throw ::std::runtime_error("Error serializing test result: stream is not good");
      }
    }
    catch (const ::std::runtime_error& e) {
      throw;
    }
    catch (...) {
      throw ::std::runtime_error("Unknown exception raised trying to serialize TestResult");
    }
  }

  ::timber::Reference<TestResult> TestResult::read (::std::istream& stream) throws (::std::runtime_error)
  {
    try {
#if 0
      ::inquest::xml::dom::Document doc = ::inquest::xml::parser::DOMParser::createDocument(stream);
      ::inquest::xml::dom::Element e = doc.rootElement();
      return create(e);
#endif
      throw ::std::runtime_error("Cannot read");
    }
    catch (const ::std::runtime_error& e) {
      throw;
    }
    catch (...) {
      throw ::std::runtime_error("Unknown exception raised trying to read a TestResult");
    }
  }
}

#ifndef _INQUEST_TESTSUITE_H
#define _INQUEST_TESTSUITE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _INQUEST_TESTRUNNER_H
#include <inquest/TestRunner.h>
#endif

#ifndef _INQUEST_TESTRESULTS_H
#include <inquest/TestResults.h>
#endif

namespace inquest {
  /**
   * A test suite is a container for tests and runs 
   * each test with a a test runner.
   */
  class TestSuite : public ::timber::Counted {
    /** No copying allowed */
    TestSuite(const TestSuite&);
    TestSuite&operator=(const TestSuite&);
     
    /** Default constructor */
  protected:
    TestSuite () throws ();
      
    /** The destructor */
  protected:
    ~TestSuite() throws ();
      
    /**
     * Run each test in this test suite.
     * @param runner the test runner
     * @param results the results will be recorded here
     * @throws an exception if an error occurred
     */
  public:
    virtual void doTests(::timber::Reference<TestRunner> runner, ::timber::Reference<TestResults> results) = 0;
  };
}


#endif

#ifndef _INQUEST_TESTRESULTS_H
#define _INQUEST_TESTRESULTS_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _INQUEST_TESTRESULT_H
#include <inquest/TestResult.h>
#endif

namespace inquest {
  /**
   * TestResults holds the results from running multiple tests.
   */
  class TestResults : public ::timber::Counted {
    /** No copying allowed */
    TestResults(const TestResults&);
    TestResults&operator=(const TestResults&);
    
    /** 
     * Default constructor 
     */
  public:
    TestResults() throws ();
    
    /** The destructor */
  public:
    ~TestResults() throws ();
    
    /**
     * Add a test result.
     * @param result a test result
     */
  public:
    virtual void addResult (::timber::Reference<TestResult> result) = 0;
  };
}


#endif

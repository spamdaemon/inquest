#include <inquest/SimpleTestRunner.h>
#include <inquest/NoResultException.h>

namespace inquest {
  SimpleTestRunner::SimpleTestRunner () throws()
  {}
  
  SimpleTestRunner::~SimpleTestRunner() throws()
  {}
    
  ::timber::Reference<TestResult> SimpleTestRunner::run(::timber::Reference<TestCase> tc)
  {
    ::timber::Pointer<TestResult> result;
    
    try {
      tc->doTest();
      result = TestResult::create(tc,TestState::PASSED);
    }
    catch (const NoResultException& e) {
      result =TestResult::create(tc,TestState::UNKNOWN,"No result");
    }
    catch (const ::std::exception& e) {
      result =TestResult::create(tc,TestState::FAILED,"Uncaught Exception ",e);
    }
    catch (...) {
      result = TestResult::create(tc,TestState::FAILED,"Uncaught Exception");
    }

    switch (tc->expectedOutcome().value()) {
    case TestState::PASSED:
      break;
    case TestState::FAILED:
      if (result->outcome()==TestState::PASSED) {
	result = TestResult::create(tc,TestState::FAILED,"Expected test failure, but passed test");
      }
      else if (result->outcome()==TestState::FAILED) {
	result = TestResult::create(tc,TestState::PASSED);
      }
      break;
    case TestState::UNKNOWN:
      if (result->outcome()==TestState::UNKNOWN) {
	result = TestResult::create(tc,TestState::PASSED);
      }
      else {
	result = TestResult::create(tc,TestState::FAILED,"Expected no result");
      }
      break;
    default:
      throw ::std::logic_error("Invalid expected outcome");
    }
      
    return result;
  }
    
}

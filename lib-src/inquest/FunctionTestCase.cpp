#include <inquest/FunctionTestCase.h>

namespace inquest {
  FunctionTestCase::FunctionTestCase (Function f, const ::std::string& tid) throws(::std::invalid_argument)
    : _function(f),_id(tid)
  {
    if(f==0) {
      throw ::std::invalid_argument("No function");
    }
    if (tid.empty()) {
      throw ::std::invalid_argument("No test case id");
    }
    
  }
  
  FunctionTestCase::~FunctionTestCase() throws()
  {}
  
  void FunctionTestCase::doTest()
  { (*_function)(); }
  
  ::std::string FunctionTestCase::id() const throws()
  { return _id; }
  
}

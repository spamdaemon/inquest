#ifndef _INQUEST_TESTRUNNER_H
#define _INQUEST_TESTRUNNER_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _INQUEST_TESTCASE_H
#include <inquest/TestCase.h>
#endif

#ifndef _INQUEST_TESTRESULT_H
#include <inquest/TestResult.h>
#endif

namespace inquest {
  /**
   * The test runner is used to execute a single test case
   * and monitor its outcome.
   */
  class TestRunner : public ::timber::Counted {
    /** No copying allowed */
    TestRunner(const TestRunner&);
    TestRunner&operator=(const TestRunner&);
    
    /** Default constructor */
  protected:
    TestRunner () throws ();
      
    /** The destructor */
  public:
    ~TestRunner() throws ();
      
    /**
     * Run the specified test.
     * @param tc a testcase
     * @return the result of running the test case
     * @throws any exception if there was a problem with the runner itself
     */
  public:
    virtual ::timber::Reference<TestResult> run(::timber::Reference<TestCase> tc) = 0;
  };
    
}


#endif

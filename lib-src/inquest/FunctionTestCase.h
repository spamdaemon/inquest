#ifndef _INQUEST_FUNCTIONTESTCASE_H
#define _INQUEST_FUNCTIONTESTCASE_H

#ifndef _INQUEST_TESTCASE_H
#include <inquest/TestCase.h>
#endif

#include <string>
#include <stdexcept>

namespace inquest {
  /**
   * A test case is used to test a function or class method with 
   * a specific set of inputs and a defined set of outputs.
   */
  class FunctionTestCase : public TestCase {
    /** No copying allowed */
    FunctionTestCase(const FunctionTestCase&);
    FunctionTestCase&operator=(const FunctionTestCase&);
    
    /** A function */
  public:
    typedef void (*Function)();
    
    /**
     * Create a new testcase for the specified function.
     * @param f a function
     * @param tid the test id
     * @throws ::std::invalid_argument if f==0 || tid.empty()
     */
  public:
    FunctionTestCase (Function f, const ::std::string& tid) throws (::std::invalid_argument);
    
    /** The destructor */
  public:
    ~FunctionTestCase() throws ();
    
    /**
     * Execute this test case. 
     * @throws any exception if no result could be obtained
       */
  public:
    void doTest();
    
    /**
     * Get the id
     * @return the id
     */
  public:
    ::std::string id() const throws ();
    
    /** The function to be tested */
  private:
    Function _function;
    
    /** The test id */
  private:
    const ::std::string _id;
  };
  
}


#endif

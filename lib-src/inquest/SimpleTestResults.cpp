#include <inquest/SimpleTestResults.h>

namespace inquest {
  SimpleTestResults::SimpleTestResults() throws()
  {}
  
  SimpleTestResults::~SimpleTestResults() throws()
  {}
  
  void SimpleTestResults::addResult (::timber::Reference<TestResult> result)
  {
    _results.push_back(result);
  }
  
  ::std::vector< ::timber::Reference<TestResult> > SimpleTestResults::exportResults() const throws()
  { return ::std::vector< ::timber::Reference<TestResult> >(_results); }
  
}

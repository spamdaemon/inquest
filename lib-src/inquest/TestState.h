#ifndef _INQUEST_TESTSTATE_H
#define _INQUEST_TESTSTATE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

namespace inquest {
  /**
   * A test case is used to test a function or class method with 
   * a specific set of inputs and a defined set of outputs.
   */
  class TestState {
      
    /** The TestState of the test testcase */
  public:
    enum State {
      PASSED, //< the testcase passed
      FAILED, //< the testcase failed
      UNKNOWN //< the testcase TestState is undefined
    };
      
    /**
     * Create an TestState
     * @param out the TestState
     */
  public:
    inline TestState (State out) : _testState(out) {}
	
      /**
       * Get the TestState.
       * @return the TestState
       */
  public:
    inline State value() const throws () { return _testState; }

    /** The equality operator */
  public:
    inline bool operator==(const TestState& o) const throws () 
      { return _testState==o._testState; }

    /** The inequality operator */
  public:
    inline bool operator!=(const TestState& o) const throws () 
{ return _testState!=o._testState; }

    /** The TestState */
  private:
    State _testState;
  };
}

#endif

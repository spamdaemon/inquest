#ifndef _INQUEST_SIMLPETESTRUNNER_H
#define _INQUEST_SIMPLETESTRUNNER_H

#ifndef _INQUEST_TESTRUNNER_H
#include <inquest/TestRunner.h>
#endif

namespace inquest {
  /**
   * A test runner is used to test a function or class method with 
   * a specific set of inputs and a defined set of outputs.
   */
  class SimpleTestRunner : public TestRunner {
    /** No copying allowed */
    SimpleTestRunner(const SimpleTestRunner&);
    SimpleTestRunner&operator=(const SimpleTestRunner&);
    
    /** Default constructor */
  public:
    SimpleTestRunner () throws ();
    
    /** The destructor */
  public:
    ~SimpleTestRunner() throws ();
    
    /**
     * Runs the specified test case.
     * @param tc a testcase
     * @return the a test result
     * @throws any exception if there was a problem creating the result
     */
  public:
    ::timber::Reference<TestResult> run(::timber::Reference<TestCase> tc);
  };
  
}


#endif

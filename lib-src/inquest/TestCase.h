#ifndef _INQUEST_TESTCASE_H
#define _INQUEST_TESTCASE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _INQUEST_TESTSTATE_H
#include <inquest/TestState.h>
#endif

#include <string>

namespace inquest {
    /**
     * A test case is used to test a function or class method with 
     * a specific set of inputs and a defined set of outputs.
     */
  class TestCase : public ::timber::Counted {
    /** No copying allowed */
  private:
    TestCase(const TestCase&);
    TestCase& operator= (const TestCase&);
    
    /** Default constructor */
  protected:
    TestCase () throws ();
    
    /** The destructor */
  public:
    ~TestCase() throws ();
    
    /**
     * A unique ID for this test case
     * @return a string that identifies this testcase
     */
  public:
    virtual ::std::string id() const throws () = 0;
    
    /**
     * Execute this test case. 
     * @throws any exception if no result could be obtained
     */
  public:
    virtual void doTest() = 0;
    
    /**
     * Get the expected outcome.
     * @return defaults to TestState::PASSED
     */
  public:
    virtual TestState expectedOutcome() const throws ();
  };
}

#endif

# these are recommended in gnu make manual
SHELL := /bin/sh
COMMA := ,
EMPTY :=
SPACE := $(empty) $(empty)

.DEFAULT_GOAL := all

PROJECT_DIR := $(abspath $(CURDIR))

# we always export project dir to subshells
export PROJECT_DIR

# the optimization flags are automatically exported
export OPTIMIZE

BASE_DIR := $(PROJECT_DIR)

# get this project's information
include $(PROJECT_DIR)/build/pinfo.mk

# determine which external libraries we're using
include $(PROJECT_DIR)/build/ext.mk

# get the projects on which this project depends
include $(PROJECT_DIR)/build/proj.mk

# make sure the basic info about the projects on which
# this project depends are defined
ifndef PROJ_LIB_SEARCH_PATH
	PROJ_LIB_SEARCH_PATH := $(addsuffix /lib,$(PROJ_DIRS))
endif
ifndef PROJ_LINK_LIBS
PROJ_LINK_LIBS := $(addprefix -l,$(PROJ_NAMES))
endif

ifndef PROJ_INC_SEARCH_PATH
PROJ_INC_SEARCH_PATH := $(addsuffix /lib-src,$(PROJ_DIRS))
endif

ifndef PROJ_COMPILER_DEFINES
PROJ_COMPILER_DEFINES :=
endif


# how are we testing
include $(PROJECT_DIR)/build/test.mk

# we need to include the various options for compilation (1 for each supported language)
include $(PROJECT_DIR)/build/opts.*.mk

# options needed for supporting code coverage
include $(PROJECT_DIR)/build/coverage.opts.mk

# the compilation instructions (1 for each supported language)
include $(PROJECT_DIR)/build/compile.*.mk

# the link instructions
include $(PROJECT_DIR)/build/link.mk

# include the generated sources
include $(PROJECT_DIR)/build/gen_sources.mk

# here we're finding all the source in the current project
# let's find all the sources 
include $(PROJECT_DIR)/build/sources.mk

# collect dependencies
include $(PROJECT_DIR)/build/depends.mk

# here we should make sure that the number of OBJECT_FILES equals the
# number of OBJECT_SOURCE_FILES. They might be different, if there are 
# source whose names only differ in the extensions!
include $(PROJECT_DIR)/build/common-rules.mk
include $(PROJECT_DIR)/build/common-targets.mk

include $(PROJECT_DIR)/build/rules.mk

# the targets
include $(PROJECT_DIR)/build/targets.mk

# make new modules
include $(PROJECT_DIR)/build/newmodule.mk

# include a target to make the documentation
include $(PROJECT_DIR)/build/docs.mk

# include local targets, but don't complain if it doesn't exist
-include $(PROJECT_DIR)/local.mk

#include <canopy/Process.h>
#include <canopy/dso/DSO.h>
#include <canopy/dso/SymbolTable.h>
#include <canopy/mt/Thread.h>
#include <canopy/time/Time.h>

#include <timber/w3c/xml/xml.h>
#include <timber/w3c/xml/Parser.h>
#include <timber/w3c/xml/DocumentHandler.h>

#include <iostream>
#include <stdexcept>
#include <sstream>
#include <fstream>
#include <map>
#include <memory>

using namespace ::std;
using namespace ::canopy;
using namespace ::canopy::time;
using namespace ::canopy::dso;
using namespace ::canopy::mt;

static bool emitCDATA=true;

struct Output
{
      Output(istream& in)
            : _stream(in)
      {
      }

      string _data;
      ostringstream _ostream;
      istream& _stream;
};

static unique_ptr< Process> filter;

struct TestResult
{
      enum Enum
      {
         PASSED, FAILED, TIMEOUT, NO_RESULT
      };
};

static string findSelf(const string& self, const string& argv0)
{
   if (self.empty()) {
      try {
         return Process::findExecutable();
      }
      catch (...) {
         cerr << "Could not find the path to this executable; using " << argv0 << endl;
         return argv0;
      }
   }
   return self;
}

static string filterName(const string& name)
{
   if (filter.get()) {
      filter->cin() << name << endl;
      string prettyName;
      getline(filter->cout(), prettyName);
      string::size_type pos = prettyName.find_first_of('(');
      ::std::cerr << "filterName('"<<name<<"') = '" << prettyName << "'" << ::std::endl;
      if (pos != string::npos) {
         prettyName = prettyName.substr(0, pos);
      }
      return prettyName;
   }
   return name;
}

static void consumeStream(void* in)
{
   Output& out = *reinterpret_cast< Output*>(in);

   char buf[1024];

   while (true) {
      out._stream.read(buf, sizeof(buf));
      if (out._stream.gcount() > 0) {
         out._ostream.write(buf, out._stream.gcount());
      }
      else {
         break;
      }
   }
   out._data = out._ostream.str();
   out._ostream.str("");
}

static bool isolateTest(const string& valgrind, const string& prog, const string& dso, const string& function,
     ::std::shared_ptr<map< string, string>> environment, UInt32 timeoutSec, int& nFailed, ostream& result, bool expectToPass)
{
   bool success = false;

   result << "<test>" << endl << "  <function><![CDATA[" << function << "]]></function>" << endl
         << "  <object><![CDATA[" << dso << "]]></object>" << endl;

   string prettyFunc = filterName(function);
   if (prettyFunc != function) {
      result << "  <pretty-function>" << prettyFunc << "</pretty-function>" << endl;
   }

   ostringstream resultStream;

   try {
      ::canopy::Process::Command command;
     command.environment=environment;

      string executable;
      if (valgrind.empty()) {
         command.path = prog;
      }
      else {
         command.path = valgrind;
         command.arguments.push_back("--tool=memcheck");
         command.arguments.push_back(prog);
      }
      command.arguments.push_back("-test");
      command.arguments.push_back(function);
      command.arguments.push_back("-object");
      command.arguments.push_back(dso);

      resultStream << "<command>" << command.path;
      for (auto& arg : command.arguments) {
         resultStream << ' ' << arg;
      }
      resultStream << "</command>" << endl;

      if (command.environment) {
         resultStream << "<environment>" << endl;
         for (auto& entry : *command.environment) {
            resultStream << "<binding " << "name='" << ::timber::w3c::xml::escape(entry.first) << "' " << "value='"
                  << ::timber::w3c::xml::escape(entry.second) << "'/>" << endl;
         }
         resultStream << "</environment>" << endl;
      }

      const Time startTime = Time::now();
      Process proc = Process::execute(command);

      Output cerrOut(proc.cerr());
      Output coutOut(proc.cout());

      Thread cerrReader("cerr-reader", consumeStream, &cerrOut);
      Thread coutReader("cout-reader", consumeStream, &coutOut);

      auto timeoutNS = ::std::chrono::duration_cast < Time::Duration > (::std::chrono::seconds(timeoutSec));

      bool timeout = true;
      Int64 sleepTime = 10000000; // sleep for 0.01s
      while ((Time::now().time() - startTime.time()) < timeoutNS) {
         Thread::suspend(sleepTime);
         // check the process status first
         if (proc.state() != Process::RUNNING) {
            break;
         }
         // sleep longer again, but this time twice as long (up to a maximum)
         sleepTime = min(2 * sleepTime, INT64_C(1000000000));
      }

      if (proc.state() != Process::RUNNING) {
         timeout = false;
      }
      else {
         try {
            proc.terminate();
            proc.waitFor();
         }
         catch (...) {
            cerr << "Could not terminate process";
         }
      }
      const Time endTime = Time::now();

      // wait for readers to complete
      Thread::join(cerrReader);
      Thread::join(coutReader);

      {
         char timeBuf[Time::BUFFER_SIZE];
         startTime.format(timeBuf, 6);
         resultStream << "  <start-time>" << timeBuf << "</start-time>" << endl;
      }

      if (!valgrind.empty()) {
         resultStream << "  <valgrind><![CDATA[" << valgrind << "]]></valgrind>" << endl;
      }
      resultStream << "  <execution-time>" << (endTime.time() - startTime.time()).count() << "</execution-time>"
            << endl;

      resultStream << "  <stdout>";
      if (!coutOut._data.empty()) {
    	  if (emitCDATA) {
    		  resultStream << "<![CDATA[" << ::std::endl << coutOut._data << ::std::endl << "]]>";
    	  }
    	  else {
    		  resultStream << ::timber::w3c::xml::escape(coutOut._data);
    	  }
      }
      resultStream << "</stdout>" << endl;

      resultStream << "  <stderr>";
      if (!cerrOut._data.empty()) {
    	  if (emitCDATA) {
    		  resultStream << "<![CDATA[" << ::std::endl << cerrOut._data << ::std::endl << "]]>";
    	  } else {
    		  resultStream << ::timber::w3c::xml::escape(cerrOut._data);
    	  }
      }
      resultStream << "</stderr>" << endl;

      TestResult::Enum testResult = TestResult::NO_RESULT;

      if (timeout) {
         testResult = TestResult::TIMEOUT;
         resultStream << "  <result>TIMEOUT</result>" << endl;
      }
      else if (proc.state() == Process::EXITED) {
         if (proc.exitCode() == 0) {
            testResult = TestResult::PASSED;
         }
         else if (proc.exitCode() == 1) {
            testResult = TestResult::FAILED;
         }
         else if (proc.exitCode() == 2) {
            testResult = TestResult::NO_RESULT;
         }
         else {
            testResult = TestResult::FAILED;
         }
      }
      else if (proc.state() == Process::TERMINATED) {
         testResult = TestResult::FAILED;
      }
      else {
         testResult = TestResult::NO_RESULT;
      }

      // invert the result for success in case we're not expecting to pass
      if (testResult == TestResult::PASSED && !expectToPass) {
         testResult = TestResult::FAILED;
      }
      else if (testResult == TestResult::FAILED && !expectToPass) {
         testResult = TestResult::PASSED;
      }

      if (testResult == TestResult::PASSED) {
         success = true;
      } else {
	++nFailed;
	success = false;
      }

      resultStream << "   <result>";
      switch (testResult) {
         case TestResult::PASSED:
            resultStream << "OK";
            break;
         case TestResult::FAILED:
            resultStream << "FAILURE";
            break;
         case TestResult::TIMEOUT:
            resultStream << "TIMEOUT";
            break;
         case TestResult::NO_RESULT:
            resultStream << "NO RESULT";
            break;
      };

      resultStream << "</result>" << endl;
   }
   catch (...) {
      resultStream.str("");
   }

   result << resultStream.str();
   result << "</test>" << endl << flush;

   return success;
}

static int runTestSuite(const string& valgrind, const string& self, const string& passTestFunc,
      const string& failTestFunc, UInt32 timeout, ::std::shared_ptr<map< string, string>> environment, const string& dso,
      ostream& resultStream)
{
   int nFailed = 0;
   try {
      unique_ptr< SymbolTable> symtab = SymbolTable::load(dso);
      set < string > symbols = symtab->names();
      for (set< string>::const_iterator i = symbols.begin(); i != symbols.end(); ++i) {
         if (symtab->symbolType(*i) == SymbolTable::FUNCTION_SYMBOL) {
            if (!passTestFunc.empty() && i->find(passTestFunc) != string::npos) {
               isolateTest(valgrind, self, dso, *i, environment, timeout, nFailed, resultStream, true);
            }
            else if (!failTestFunc.empty() && i->find(failTestFunc) != string::npos) {
               isolateTest(valgrind, self, dso, *i, environment, timeout, nFailed, resultStream, false);
            }
         }
      }
   }
   catch (...) {
      cerr << "Cannot load symbol table for " << dso << endl;
   }
   return nFailed;
}

enum UpdateType
{
   ALL, FIND_REGRESSIONS, UPDATE_FAILED
};

static int updateReport(const string& valgrind, const string& prog, UpdateType updateType, int timeout, istream& input,
      ostream& result)
{
   struct Handler : public ::timber::w3c::xml::DocumentHandler
   {
         Handler(const string& vgrind, const string& xprog, UpdateType xupdateType, UInt32 to, ostream& xresult)
               : _valgrind(vgrind), _program(xprog), _output(xresult), _current(0), _updateType(xupdateType), _timeout(
                     to), _nFailed(0),_ok(false),_useValgrind(false)
         {
         }

         ~Handler() throws()
         {
         }

         void notifyStartElement(const char* name, const char*, const char*)
         {
            string e(name);
            if (e == "report") {
               return;
            }

            if (e == "valgrind") {
               _current = &_valgrindPath;
               _valgrindPath = "";
            }
            else if (e == "test") {
               _current = 0;
               _ok = true;
               _elementContent.str("");
            }
            else if (e == "function") {
               _current = &_function;
               _function = "";
            }
            else if (e == "object") {
               _current = &_object;
               _object = "";
            }
            else if (e == "result") {
               _current = &_result;
               _result = "";
            }
            _elementContent << "<" << e << ">";
         }

         void notifyEndElement(const char* name, const char*, const char*)
         {
            string e(name);
            if (e == "report") {
               return;
            }
            if (e == "valgrind") {
               if (!_valgrind.empty()) {
                  // override the valgrind path in the file with that from the command line
                  _valgrindPath = _valgrind;
               }
            }
            else if (e == "test") {
               bool rerun;
               switch (_updateType) {
                  case UPDATE_FAILED:
                     rerun = _result != "OK";
                     break;
                  case FIND_REGRESSIONS:
                     rerun = _result == "OK";
                     break;
                  default:
                     rerun = true;
                     break;
               }

               if (!rerun) {
                  _output << _elementContent.str() << "</" << e << ">" << endl;
               }
               else {
                  if (!_object.empty() && !_function.empty()) {
                     ostringstream tmpBuf;
                     ::std::shared_ptr<map < string, string >> noEnv;
                     bool success = isolateTest(_valgrindPath, _program, _object, _function, noEnv, _timeout, _nFailed,
                           tmpBuf, true);
                     if (_updateType == FIND_REGRESSIONS && success) {
                        assert(_result == "OK");
                        _output << _elementContent.str() << "</" << e << ">" << endl;
                     }
                     else {
                        _output << tmpBuf.str();
                     }
                     _object = _function = "";
                  }
               }
               _elementContent.str("");
            }
            else {
               _elementContent << "</" << e << ">";
            }
            _current = 0;
         }

         void notifyCharacterData(const char* txt, int count)
         {
            if (_current != 0) {
               _current->append(txt, count);
            }
            _elementContent.write(txt, count);
         }

      public:
         string _valgrind;
         string _program;
         ostream& _output;

         string* _current;
         string _object;
         string _function;
         string _result;
         ostringstream _elementContent;
         UpdateType _updateType;
         UInt32 _timeout;
         int _nFailed;
      private:
         bool _ok;
         bool _useValgrind;
         string _valgrindPath;
   };

   unique_ptr< ::timber::w3c::xml::Parser> parser = ::timber::w3c::xml::Parser::create();
   Handler handler(valgrind, prog, updateType, timeout, result);
   parser->parseStream(input, handler);
   return handler._nFailed;
}

static void runTest(const string& dso, const string& function)
{
   typedef void (*FunctionPointer)();

   unique_ptr< DSO> obj;
   try {
      obj = DSO::load(dso.c_str(), true);
   }
   catch (const exception& e) {
      cerr << "Could not load DSO " << dso << endl << "NO_RESULT " << e.what() << endl;
      exit(2);
   }

   FunctionPointer f = 0;
   try {
      f = reinterpret_cast< FunctionPointer>(obj->symbol(function.c_str()));
   }
   catch (...) {
   }

   if (f == 0) {
      cerr << "NO_RESULT symbol " << function.c_str() << " not found" << endl;
      exit(2);
   }

   try {
      f();
      exit(0);
   }
   catch (const exception&) {
      cerr << "FAILURE exception" << endl;
      exit(1);
   }
   catch (...) {
      cerr << "FAILURE unknown exception" << endl;
      exit(1);
   }
}

static void printUsageAndExit(int exitCode)
{
   cerr << "Usage: [options] [test-files]" << endl << " Program options" << endl
         << "  -exe <path to executable>        (optional) path to the inquest executable for recursive invocation"
         << endl
         << "  -substr-pass <substr-expression>       substring of the (mangled) function to be invoked and which is expected to pass"
         << endl
         << "  -substr-fail <substr-expression>  substring of the (mangled) function to be invoked and which is expected to fail"
         << endl << "  -substr <substr-expression>       same as -substr-pass" << endl
         << "  -update-regressions <test-output> run only tests on regressions" << endl
         << "  -update-all <test-output>         run all tests previously run" << endl
         << "  -update-failed <test-output>      run all tests that previously failed" << endl
         << "  -c++filt <path>                   path to a C++ symbol demangler" << endl
         << "  -valgrind <path>                  path to valgrind which will be run with --tool=memcheck" << endl
         << "  -timeout <sec>                    time after which to abort the test (at least 1, default 60)" << endl
         << "  -fail-on-error                    return a non-zero status if a test failed" << endl
         << "  -env NAME=VALUE                   define an environment variable NAME with an optional value VALUE"
         << endl << "                                    (use multiple times if injecting multiple variables)" << endl
         << "  -environment [inherit | clean]    inherit the environment or discard it" << endl
         << "                                    (use multiple times if injecting multiple variables)" << endl
         << " Internally used options (not meant for external use)" << endl
         << "  -test <test>                      execute the specified test function (must be specified with -object)"
         << endl << "  -object <test-object>             execute only the specified test (must be specified with -test)"
         << endl << endl << "  -s  can be used instead of -substr" << endl << "  -t  can be used instead of -timeout"
         << endl << endl << "Compile example with GCC : gcc -fPIC -shared <test> -o <test>.tst" << endl << endl
         << "Debugging with inquest" << endl << "  1. gdb inquest" << endl
         << "  2. run -test <test> -object <test-object>" << endl
         << " The test and the test object can be obtained from the XML output from a failed test" << endl;
   exit(exitCode);
}

static void parseEnvVar(const string& namevalue, map< string, string>& environment)
{
   string key, value;
   const char* delim = " =:";

   string::size_type start = namevalue.find_first_not_of(delim);
   string::size_type end = namevalue.find_first_of(delim);
   if (start == string::npos) {
      key = namevalue;
   }
   else {
      key = namevalue.substr(start, end - start);
      start = end + 1;
      end = namevalue.length();
      if (start < end) {
         value = namevalue.substr(start, namevalue.length() - start);
      }
   }
   environment[key] = value;
}

int main(int argc, char** argv)
{
   string valgrind;
   string testFunction;
   string testObject;
   vector < string > objects;
   string cppfilt;
   string update;
   string functionSubstring("inquest_");
   string functionSubstringFail("inquestfail_");
   UInt32 timeout = 60;
   UpdateType updateType = ALL;
   bool failOnError = false;
   string self;
   map < string, string > environment;
   bool inheritEnvironment = true;

   if (argc == 1) {
      printUsageAndExit(1);
   }

   try {
      for (int i = 1; i < argc; ++i) {
         const string arg(argv[i]);
         if (arg == "") {
            throw runtime_error("Invalid argument ''");
         }
         else if (arg == "-h") {
            printUsageAndExit(0);
         }
         else if (arg == "-fail-on-error") {
            failOnError = true;
         }
         else if (arg == "-environment") {
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument " + string(arg));
            }
            string inherit(argv[i]);
            inheritEnvironment = inherit != "inherit";
         }
         else if (arg == "-timeout" || arg == "-t") {
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument " + string(arg));
            }
            istringstream in(argv[i]);
            in >> timeout;
            if (in.fail()) {
               throw runtime_error("Could not read argument " + string(argv[i]) + " for " + arg);
            }
            if (timeout < 1) {
               throw runtime_error("Invalid timeout value specified " + string(argv[i]));
            }
         }
         else if (arg == "-substr" || arg == "-substr-pass" || arg == "-s") {
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument " + string(arg));
            }
            functionSubstring = argv[i];
            if (functionSubstring.empty()) {
               throw runtime_error("Empty function prefix subsr");
            }
         }
         else if (arg == "-substr-fail") {
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument " + string(arg));
            }
            functionSubstringFail = argv[i];
            if (functionSubstringFail.empty()) {
               throw runtime_error("Empty function prefix for substrFail");
            }
         }
         else if (arg == "-update-regressions" || arg == "-update-all" || arg == "-update-failed") {
            if (filter.get() != 0) {
               throw runtime_error("Report for update already specified");
            }
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument " + string(arg));
            }
            update = argv[i];
            if (update.empty()) {
               throw runtime_error("No report file specified");
            }
            if (arg == "-update-regressions") {
               updateType = FIND_REGRESSIONS;
            }
            else if (arg == "-update-all") {
               updateType = ALL;
            }
            else if (arg == "-update-failed") {
               updateType = UPDATE_FAILED;
            }
         }
         else if (arg == "-env") {
            if (++i < argc) {
               parseEnvVar(argv[i], environment);
            }
         }
         else if (arg == "-c++filt") {
            if (filter.get() != 0) {
               throw runtime_error("Filter already specified");
            }
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument " + string(arg));
            }
            ::canopy::Process::Command filterCommand;
            filterCommand.path = argv[i];
            filter.reset(new Process(Process::execute(filterCommand)));
            if ("X" != filterName("X") || filter->state() != Process::RUNNING) {
               throw runtime_error("Could not start c++filt " + filterCommand.path);
            }
         }
         else if (arg == "-valgrind") {
            if (filter.get() != 0) {
               throw runtime_error("Valgrind already specified");
            }
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument " + string(arg));
            }
            valgrind = argv[i];
         }
         else if (arg == "-test") {
            if (!testFunction.empty()) {
               throw runtime_error("Test function already specified");
            }
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument -test");
            }
            testFunction = argv[i];
            if (testFunction.empty()) {
               throw runtime_error("Empty test function not allowed");
            }
         }
         else if (arg == "-object") {
            if (!testObject.empty()) {
               throw runtime_error("Test object already specified");
            }
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument -object");
            }
            testObject = argv[i];
            if (testObject.empty()) {
               throw runtime_error("Empty test object not allowed");
            }
         }
         else if (arg == "-path") {
            if (!self.empty()) {
               throw runtime_error("Option exe already specified");
            }
            if (++i == argc) {
               throw runtime_error("Expected parameter for argument -path");
            }
            self = argv[i];
            if (self.empty()) {
               throw runtime_error("Empty argument not allowed");
            }
         }
         else {
            objects.push_back(argv[i]);
         }
      }

      if (update.empty()) {
         if (!objects.empty() && (!testFunction.empty() || !testObject.empty())) {
            throw runtime_error("Only specify test objects or a single test object and a test function");
         }
         if (testFunction.empty() != testObject.empty()) {
            throw runtime_error("Both -test and -object must be specified if either one is specified");
         }
      }
      else {
         if (!objects.empty() || !testFunction.empty() || !testObject.empty()) {
            throw runtime_error("Only specify test objects or a single test object and a test function");
         }
      }
   }
   catch (const exception& e) {
      cerr << "Exception " << e.what() << endl;
      printUsageAndExit(1);
   }

   // check the environment
   map < string, string > oldEnvironment;
   shared_ptr<map < string, string >> passEnv;
   if (inheritEnvironment) {
      // FIXME: fill in old environment
      oldEnvironment.insert(environment.begin(), environment.end());
      passEnv = nullptr; //FIXME: needs to pass oldEnvironment
   }
   else {
      passEnv = make_shared<map < string, string >>(environment);
   }

   int exitCode = 0;
   if (!update.empty()) {
      self = findSelf(self, argv[0]);
      ostream& out = cout;
      ifstream stream(update.c_str());
      out << "<?xml version='1.0' encoding='utf-8' ?>" << endl;
      out << "<report>" << endl;
      updateReport(valgrind, self, updateType, timeout, stream, out);
      out << "</report>" << endl;
   }
   else if (testFunction.empty()) {
      self = findSelf(self, argv[0]);
      ostream& out = cout;
      out << "<?xml version='1.0' encoding='utf-8' ?>" << endl;
      out << "<report>" << endl;
      for (vector< string>::const_iterator i = objects.begin(); i != objects.end(); ++i) {
         if (runTestSuite(valgrind, self, functionSubstring, functionSubstringFail, timeout, passEnv, *i, out) > 0
               && failOnError) {
            exitCode = 1;
         }
      }
      out << "</report>" << endl;
   }
   else {
      runTest(testObject, testFunction);
   }

   if (filter.get() != 0) {
      filter->closeCin();
      filter->closeCerr();
      filter->closeCout();
      filter->waitFor();
   }
   cout.flush();
   cerr.flush();

   return exitCode;
}


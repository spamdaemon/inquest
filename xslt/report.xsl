<?xml version="1.0" encoding="UTF-8" ?>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output method="xml" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" 
/>

  <xsl:template match="/">
   <html xml:lang="en" lang="en">
        <head>
           <title>Test Report</title>
        </head>
        <body>
            <xsl:apply-templates/>
        </body>
    </html>
  </xsl:template>

  <xsl:template match="/report">
    <table rules="all" border="1" frame="border" cellpadding="2" cellspacing="1">
        <thead>
        <tr>
            <th>Function</th>
            <th>Object File</th>
        </tr>
        </thead>
        <xsl:apply-templates/>
    </table>
  </xsl:template>

  
  <xsl:template match="/report/test">
     <tr>
        <xsl:choose>
           <xsl:when test="string(child::result)='OK'"> 
            <xsl:attribute name="bgcolor">#77DD77</xsl:attribute>
           </xsl:when>
           <xsl:when test="string(child::result)='NO RESULT'"> 
            <xsl:attribute name="bgcolor">yellow</xsl:attribute>
           </xsl:when>
           <xsl:otherwise> 
            <xsl:attribute name="bgcolor">#DD5555</xsl:attribute>
           </xsl:otherwise>
        </xsl:choose>
       <td>
          <xsl:choose>
             <xsl:when test="child::pretty-function[node()]">
                <xsl:value-of select="./pretty-function"/>
             </xsl:when>
             <xsl:otherwise> 
               <xsl:value-of select="./function"/>
             </xsl:otherwise> 
           </xsl:choose>
       </td>
       <td><xsl:value-of select="./object"/></td>
    </tr>
  </xsl:template> 

</xsl:stylesheet>


#include <iostream>
#include <cstdlib>

void utest_success()
{
  ::std::cerr << "OK" << ::std::endl;
}
 
void failtest_abort()
{
  ::std::cerr << "ABORT" << ::std::endl;
  ::std::abort(); 
}
 

void failtest_exit()
{
  ::std::cerr << "EXIT(1)" << ::std::endl;
  ::std::exit(1);
}

